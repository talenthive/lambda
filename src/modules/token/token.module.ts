import { TokenController } from './token.controller';
import { JwtModule } from '@nestjs/jwt';
import { TokenService } from './token.service';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Module({
    imports: [
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                secret: process.env.JWT_SECRET,
                signOptions: { expiresIn: process.env.JWT_TOKEN_LIFETIME },
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [TokenController],
    providers: [TokenService],
})
export class TokenModule {}
