import { Test } from '@nestjs/testing';
import { TokenService } from '../token.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

describe('TokenService', () => {
    let tokenService: TokenService;
    let token: string;
    let payload = {
        userId: 1002,
    };

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                JwtModule.registerAsync({
                    useFactory: async (configService: ConfigService) => ({
                        secret: process.env.JWT_SECRET,
                        signOptions: {
                            expiresIn: process.env.JWT_TOKEN_LIFETIME,
                        },
                    }),
                }),
            ],
            controllers: [],
            providers: [TokenService],
        }).compile();

        tokenService = moduleRef.get<TokenService>(TokenService);
    });

    it('Service should be defined', () => {
        expect(tokenService).toBeDefined();
    });

    it('Should generate a token', async () => {
        token = await tokenService.generateToken(payload);
        expect(token).toBeDefined();
    });

    it('Should validate the token', async () => {
        const result = await tokenService.validateToken(token);
        expect(result).toBeDefined();
        expect(result.userId).toEqual(payload.userId);
    });
});
