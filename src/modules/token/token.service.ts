import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class TokenService {
    constructor(private readonly jwtService: JwtService) {}

    async generateToken(payload): Promise<string> {
        try {
            const token = await this.jwtService.sign(payload, {
                expiresIn: process.env.JWT_TOKEN_LIFETIME,
            });
            return token;
        } catch (e) {
            throw new HttpException(
                'Cannot generate a token',
                HttpStatus.UNAUTHORIZED
            );
        }
    }

    async validateToken(token: string): Promise<any> {
        try {
            const payload = await this.jwtService.verifyAsync(token);
            return payload;
        } catch (e) {
            throw new HttpException(
                'Token is invalid',
                HttpStatus.UNAUTHORIZED
            );
        }
    }
}
