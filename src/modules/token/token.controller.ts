import { Controller } from '@nestjs/common';
import { TokenService } from './token.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class TokenController {
    constructor(private readonly tokenService: TokenService) {}

    @MessagePattern({ cmd: 'generateToken' })
    async generateToken(payload): Promise<string> {
        return await this.tokenService.generateToken(payload);
    }

    @MessagePattern({ cmd: 'validateToken' })
    async validateToken(token: string): Promise<any> {
        return await this.tokenService.validateToken(token);
    }
}
